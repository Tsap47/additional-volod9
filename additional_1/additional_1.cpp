﻿#include <iostream>

using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");
	int q1, p1, q2, p2, a, sum, sum1 = 0, sum2 = 0;
	int sum3 = 0, y = 0, multiples = 0;
	cout << "Судоходная компания предлагает два вида транспорта\nдля перевозки сыпучих грузов.Грузовик первого типа может перевезти Q1 тонн груза за одну поездку.\nРазовая поездка стоит P1, и цена не зависит от уровня загрузки транспортного средства.\nДля грузовика второго типа эти значения равны Q2 и P2 соответственно.\nВведите: Q1, P1, Q2, P2, A" << endl;
	cin >> q1 >> p1 >> q2 >> p2 >> a;
	float div1, div2;

	div1 = float(p1) / q1;
	div2 = float(p2) / q2;
	int b = a;
	int min = 10000;
	if (div1 < div2)
	{
		b = a;
		int x = b / q1;
		b -= x * q1;
		sum1 += x * p1;
	}

	else
	{
		b = a;
		int x = b / q2;
		b -= x * q2;
		sum1 += x * p2;
	}



	while (b > 0)
	{
		if (q1 < q2)
		{
			sum1 += p1;
			b -= q1;
		}
		else
		{
			sum1 += p2;
			b -= q2;
		}
	}
	if (sum1 < min)
		min = sum1;


	b = a;
	while (b > 0)
	{
		if (div1 < div2)
		{
			b -= q1;
			sum3 += p1;
		}

		else
		{
			b -= q2;
			sum3 += p2;
		}
	}
	if (sum3 < min)
		min = sum3;

	b = a;
	if (div1 < div2)
	{
		while (b > 0)
		{
			if (b % q2 == 0)
				y = b;
			b -= q1;

		}
		if (y != 0)
		{
			multiples = ((y / q2) * p2) + (((a - y) / q1) * p1);
		}

	}
	else
	{
		while (b > 0)
		{
			if (b % q1 == 0)
				y = b;
			b -= q2;
		}
		if (y != 0)
		{
			multiples = ((y / q1) * p1) + (((a - y) / q2) * p2);
		}
	}

	if ((multiples < min) && (multiples != 0))
		min = multiples;

	cout << min;

}