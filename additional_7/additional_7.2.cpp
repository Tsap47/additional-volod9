﻿#include <iostream>
#include <vector>

using namespace std;

vector <int> getTokens(int a) {
    vector <int> tokens;
    while (a > 0) {
        tokens.push_back(a % 10);
        a /= 10;
    }
    return tokens;
}

int getProduct(const vector <int>& nums) {
    int total = 1;
    for (int i = 0; i < nums.size(); i++) {
        total *= nums[i];
    }
    return total;
}

bool getRes(const vector <int>& nums) {
    bool is7 = false;
    bool isNo0 = true;
    for (int e : nums) {
        if (e == 7) {
            is7 = true;
        }
        if (e == 0) {
            isNo0 = false;
        }
    }
    return (is7 and isNo0);
}

int main() {
    int n;
    cin >> n;

    for (int i = 1; i < n; i++) {
        vector <int> tokens = getTokens(i);
        if (getProduct(tokens) != 18) {
            cout << i << " ";
            if (getRes(tokens)) cout << i << " ";
        }
    }
}