#pragma once
using namespace std;
const int length = 10000;
const int N = length * 2;
namespace ds
{
	void InputSequence(int sequence[], long long& len);
	void OutputSequence(int sequence[]);
}

