﻿
#include <iostream>
using namespace std;
int main() {
    int count = 0;
    for (int i = 0; i <= 999999; i++)
    {
        int sumLast3 = 0;
        int sumFirst3 = 0;
        for (int j = 0; j < 3; ++j)
            sumFirst3 = i / 1 % 10 + i / 10 % 10 + i / 100 % 10;
        for (int k = 3; k < 6; ++k)
            sumLast3 = i / 1000 % 10 + i / 10000 % 10 + i / 100000 % 10;
        if (sumFirst3 == sumLast3)
            count++;
    }
    cout << count << endl;
    return 0;
}